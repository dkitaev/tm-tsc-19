package ru.tsc.kitaev.tm.api.repository;

import ru.tsc.kitaev.tm.api.IRepository;
import ru.tsc.kitaev.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User removeUser(User user);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

}
