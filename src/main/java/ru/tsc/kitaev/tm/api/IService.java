package ru.tsc.kitaev.tm.api;

import ru.tsc.kitaev.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
