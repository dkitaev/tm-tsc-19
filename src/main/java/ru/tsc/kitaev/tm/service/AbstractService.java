package ru.tsc.kitaev.tm.service;

import ru.tsc.kitaev.tm.api.IRepository;
import ru.tsc.kitaev.tm.api.IService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIdException;
import ru.tsc.kitaev.tm.exception.empty.EmptyIndexException;
import ru.tsc.kitaev.tm.model.AbstractEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public E add(final E entity) {
        if (entity == null) throw new RuntimeException();
        return repository.add(entity);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) throw new RuntimeException();
        repository.remove(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(comparator);
    }

    @Override
    public E findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public E findByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findByIndex(index);
    }

    @Override
    public E removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public E removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.removeByIndex(index);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public boolean existsByIndex(final int index) {
        return repository.existsByIndex(index);
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

}
