package ru.tsc.kitaev.tm.service;

import ru.tsc.kitaev.tm.api.repository.ITaskRepository;
import ru.tsc.kitaev.tm.api.service.ITaskService;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.empty.*;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.model.Task;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(name);
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(name);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.startById(id);
    }

    @Override
    public Task startByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.startByIndex(index);
    }

    @Override
    public Task startByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.startByName(name);
    }

    @Override
    public Task finishById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.finishById(id);
    }

    @Override
    public Task finishByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (taskRepository.getSize() < index - 1) throw new TaskNotFoundException();
        return taskRepository.finishByIndex(index);
    }

    @Override
    public Task finishByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.finishByName(name);
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeStatusById(id, status);
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Task changeStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeStatusByName(name, status);
    }

}
