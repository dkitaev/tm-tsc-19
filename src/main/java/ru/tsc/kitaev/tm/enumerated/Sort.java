package ru.tsc.kitaev.tm.enumerated;

import ru.tsc.kitaev.tm.comparator.ComparatorByCreated;
import ru.tsc.kitaev.tm.comparator.ComparatorByName;
import ru.tsc.kitaev.tm.comparator.ComparatorByStartDate;
import ru.tsc.kitaev.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    START_DATE("Sort by date start", ComparatorByStartDate.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
