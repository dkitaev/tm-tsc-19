package ru.tsc.kitaev.tm.command.task;

import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-start-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start task by index...";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().startByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

}
