package ru.tsc.kitaev.tm.command.task;

import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by index...";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!serviceLocator.getTaskService().existsByIndex(index)) throw new TaskNotFoundException();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateByIndex(index, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

}
