package ru.tsc.kitaev.tm.command.project;

import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class ProjectFinishByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-finish-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish project by name...";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

}
