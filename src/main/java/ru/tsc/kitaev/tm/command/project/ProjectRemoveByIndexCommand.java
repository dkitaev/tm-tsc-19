package ru.tsc.kitaev.tm.command.project;

import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by index...";
    }

    @Override
    public void execute() {
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = serviceLocator.getProjectService().removeByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectTaskService().removeById(project.getId());
        System.out.println("[OK]");
    }

}
