package ru.tsc.kitaev.tm.constant;

public final class TerminalConst {

    public static final String HELP = "help";

    public static final String EXIT = "exit";

}
